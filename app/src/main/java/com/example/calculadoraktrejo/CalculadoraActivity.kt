package com.example.calculadoraktrejo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.calculadoraktrejo.R

class CalculadoraActivity : AppCompatActivity() {
    private lateinit var btnSumar: Button
    private lateinit var btnRestar: Button
    private lateinit var btnMulti: Button
    private lateinit var btnDividir: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnRegresar: Button

    private lateinit var txtNum1: EditText
    private lateinit var txtNum2: EditText
    private lateinit var lblTotal: TextView
    private var calculadora = Calculadora(0.0,0.0)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()

        btnSumar.setOnClickListener {btnSumar()}
        btnRestar.setOnClickListener {btnRestar()}
        btnMulti.setOnClickListener {btnMulti()}
        btnDividir.setOnClickListener {btnDividir()}

        btnLimpiar.setOnClickListener{limpiar()}
        btnRegresar.setOnClickListener{regresar()}
    }
    fun limpiar(){
        txtNum1.setText("")
        txtNum2.setText("")
        lblTotal.setText("")
    }
    fun regresar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("¿Desea regresar?")
        confirmar.setPositiveButton("Confirmar"){ dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){ dialogInterface,which-> }.show()
    }

    private fun iniciarComponentes (){
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMulti = findViewById(R.id.btnMulti)
        btnDividir = findViewById(R.id.btnDividir)

        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        lblTotal = findViewById(R.id.lblTotal)
    }
    private fun btnSumar() {
        val num1Str = txtNum1.text.toString()
        val num2Str = txtNum2.text.toString()

        if (num1Str.isNotEmpty() && num2Str.isNotEmpty()) {
            calculadora.num1 = num1Str.toDouble()
            calculadora.num2 = num2Str.toDouble()
            lblTotal.text = calculadora.suma().toString()
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnRestar() {
        val num1Str = txtNum1.text.toString()
        val num2Str = txtNum2.text.toString()

        if (num1Str.isNotEmpty() && num2Str.isNotEmpty()) {
            calculadora.num1 = num1Str.toDouble()
            calculadora.num2 = num2Str.toDouble()
            lblTotal.text = calculadora.resta().toString()
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnMulti() {
        val num1Str = txtNum1.text.toString()
        val num2Str = txtNum2.text.toString()

        if (num1Str.isNotEmpty() && num2Str.isNotEmpty()) {
            calculadora.num1 = num1Str.toDouble()
            calculadora.num2 = num2Str.toDouble()
            lblTotal.text = calculadora.multi().toString()
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show()
        }
    }

    private fun btnDividir() {
        val num1Str = txtNum1.text.toString()
        val num2Str = txtNum2.text.toString()

        if (num1Str.isNotEmpty() && num2Str.isNotEmpty()) {
            calculadora.num1 = num1Str.toDouble()
            calculadora.num2 = num2Str.toDouble()
            lblTotal.text = calculadora.dividir().toString()
        } else {
            Toast.makeText(this, "Ingrese ambos números.", Toast.LENGTH_SHORT).show()
        }
    }
}