package com.example.calculadoraktrejo

class Calculadora {
    var num1:Double=0.0
    var num2:Double=0.0
    constructor(num1:Double,num2:Double){
        this.num2 = num2
        this.num1 = num1
    }
    fun suma():Double {
        return num1 + num2
    }
    fun resta():Double {
        return num1 - num2
    }
    fun multi():Double {
        return num1 * num2
    }
    fun dividir():Double {
        var total = 0.0
        if(num2!=0.0){
            total = num1 / num2
        }
        return total
    }
}