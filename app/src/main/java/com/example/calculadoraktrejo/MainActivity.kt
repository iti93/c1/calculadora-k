package com.example.calculadoraktrejo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.calculadoraktrejo.R

class MainActivity : AppCompatActivity() {
    private lateinit var btnIngresar : Button
    private lateinit var btnRegresar : Button
    private lateinit var txtUsuario : EditText
    private lateinit var txtContraseña : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar.setOnClickListener{ingresar()
        }
        btnRegresar.setOnClickListener{cerrar()}
    }



    private fun iniciarComponentes(){
        btnRegresar = findViewById(R.id.btnRegresar)
        btnIngresar = findViewById(R.id.btnIngresar)
        txtContraseña = findViewById(R.id.txtContraseña)
        txtUsuario = findViewById(R.id.txtUsuario)
    }
    private fun ingresar(){

        // Asignar los Strings que se declararon en R.Value
        var strUsuario:String = application.resources.getString(R.string.usuario)
        var strContra:String = application.resources.getString(R.string.contraseña)

        if(txtUsuario.text.toString() == strUsuario && txtContraseña.text.toString() == strContra){
            var bundle = Bundle()
            bundle.putString("usuario",strUsuario)

            val intent = Intent(this@MainActivity, CalculadoraActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }else{
            Toast.makeText(this.applicationContext, "Usuario o Contraseña no válidos", Toast.LENGTH_LONG).show()
        }
    }

    private fun cerrar(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora")
        confirmar.setMessage("¿Desea cerrar?")
        confirmar.setPositiveButton("Confirmar"){ dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){ dialogInterface,which-> }.show()
    }

}